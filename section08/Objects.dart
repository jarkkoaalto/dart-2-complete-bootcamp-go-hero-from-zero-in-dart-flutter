void main(){

  var myDoggie = new Doggie();
  myDoggie.eat();
  print(myDoggie.color);
}

class Object{
  String color = "Orange";
  void eat(){
    print("Object eating");
  }
  void sleep(){
    print("Object sleep");
  }
}

class Doggie extends Object{
  String color = "Yellow";
  String breed;
  void eat(){
    super.eat();
      print("The Doggie eating");
    }
}
