void main(){

  var myDino = new Dino("Dino 1", "Green");
  print(myDino);
  print(myDino.breed);
  print(myDino.color);

  var myDino2 = new Dino("Dino 2","White");
  print(myDino2);
  print(myDino2.breed);
  print(myDino2.color);

}

class Mammal{
  String color;
  Mammal(String name){
    color = name;
    print("Constructor of Mammal");
  }
}

class Dino extends Mammal{
  String breed;
  Dino(String name, String color) : super(color){
    breed = name;
    print("Default Constructor");
  }
}