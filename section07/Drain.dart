void main(){
  var myDrain = new Drain();


  myDrain.percentage = 80.0;
  print(myDrain.drainName = "Locomotive"); // default setter
  print(myDrain.color = "Red and White"); // default setter
  print(myDrain.variant = 2233);

  print("Passanger percentage is : ${myDrain.percentages}");

}

class Drain{
  String drainName; // default getter
  String color;
  int variant;
  double percent;

  void set percentage(double passengers){
  percent = (passengers / 100) * 100;
  }

  double get percentages{
    return percent;
  }
}