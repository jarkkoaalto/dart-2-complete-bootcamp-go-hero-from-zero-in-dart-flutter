void main(){
 var myCar = new Car();

  // Csr Class default values
  print(myCar.carName);
  print(myCar.color);
  print(myCar.varient);

  print(myCar.carName ="Honda");
  print(myCar.color ="Yellow");
  print(myCar.varient = 4);

  myCar.start();
  myCar.start();
  print(myCar.isRunning());


  var myCar2 = new Car();
  myCar2.start();
  myCar2.stop();
  myCar2.isRunning();
}

class Car{
  String carName = "Car Class name";
  String color  = "Car class color";
  int varient = 0;

  void start(){
    print("The $carName has started");
  }
  void stop(){
    print("The $carName has stopped now");
  }

  String isRunning(){
    return "$carName is running";
  }
}