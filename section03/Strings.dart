void main(){
  // String is basically a data type which contains text
  String word = "Hi There";
  String alias = "Harry Potter";
  String age = "21";
  print(word+","+alias+" You are now "+age);
  print(age.runtimeType);
  int x = int.parse(age);
  print(x.runtimeType);

}