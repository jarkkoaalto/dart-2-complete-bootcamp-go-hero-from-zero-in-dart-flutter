# dart-2-complete-bootcamp-go-hero-from-zero-in-dart-flutter

After completing this course you can confidently write Dart in your programming resume. this course is more than enough if you want to learn dart for using it in Flutter.

Course content:

###### Section 2: Introduction to the Dart Programming Language
###### Section 3: Basics of Dart
###### Section 4: Project 1 :- Creating a Robot
###### Section 5: Control Flow in Dart
###### Section 6: Project 2 :- User Authentication
###### Section 7: Intro to Object Oriented Programming in Dart
###### Section 8: Diving deep in Object Oriented Programming