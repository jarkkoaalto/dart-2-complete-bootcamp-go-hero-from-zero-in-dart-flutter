void main(){
  myInfo("Harry", "Potter", age: 19);
}

myInfo(String name, String lastname, {int age = 32}){
  print("Your first name is : $name");
  print("Your last name is : $lastname");
  print("Your age is:  $age" );
}
