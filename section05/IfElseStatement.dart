// If else statement in Dart

void main(){

  var myPass = "12345678";
  if(myPass.length >= 8) {
    print("Create Account");
  }
  else {
    print("Password is too short. Length of the password must be atleast 8");
  }


  String myName = "Harry Potter";
  int myAge = 11;
  if(myAge >= 18){
    print("You are adult");
  }else if(myAge == 15){
    print("You are lucky");
  }else{
    print("$myName you are too young");
  }



  // as, is, is!(not is)
  int number = 1;
  print(number is String); // false
  String number1 = "1";
  print(number1 is String); // true
  print(number != String); // true
}