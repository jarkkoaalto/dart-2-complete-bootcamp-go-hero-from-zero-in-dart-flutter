void main(){
  String name = sayHello("Harry Potter");
  print(name);
}

sayHello(String name) => "Hello $name";
// OR
// {
// return "Hello $name";
// }