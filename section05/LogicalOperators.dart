void main(){
  /*
  ! = not
  || = or
  && = and
   */

  int marks = 100;
  // not is
  if(marks != 100){
    print("Marks != 100");
  }else{
    print("Marks == 100");
  }
 // or
  if(marks == 100 || marks > 100){
    print("First condition is true");
  }else{
    print("Our assumption is Wrong");
  }
  // And
  int pass = 12345678;
  if(pass is int && pass >= 8){
    print("Valid Pass");
  }else{
    print("Invalid pass");
  }
}